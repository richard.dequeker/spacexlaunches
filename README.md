# Space X Launches 🚀

Space x Launches est une application qui collecte les informations des lancements prochain et passé de space x.

Elle a été conçu en Xamarin pour permettre à nos équipes un dévellopement rapide et multi-plateformes, tout en respectant la ligne de conduite établie par le Material Design ce qui donne un design propre et élégant à notre application.

## Architecture

Nous avons choisi d'utiliser Xamarin.Forms car il nous fourni plus de 40 contrôles que l'on peut combiner pour créer des interfaces utilisateur multi-platefromes natives avec un seul code centraliser.

Prism is a framework for building loosely coupled, maintainable, and testable XAML applications in 

De plus nous avons utiliser le framework Prism pour un dévellopement plus maintenable et testable d'application XAML en Xamarin.Forms basé sur le pattern MVVM. 

### Portable Class Library

- Data
  - Dossier qui rassemble tous les services de l'application me permettant de lire, ecrire mes données.

- Models
  - Dossier qui rassemble tous mes models pour représenter mes données.
- Resources
  - Dossier qui rassemble toutes mes vues générique et réutilisable pour éviter la duplication de code.

- ViewModels
  - Dossier qui rassemble tous les view model, elles permettent de controler l'interface.

- Views
  - Dossier qui rassemble toutes les view, elle sont l'interface, elle sont controller par un view models.

- App
  - Fichier ou je défini la navigation et les resources globales.
- Constants
  - Fichier ou je défini les constantes de l'application.

## Ergonomie

L'application se veux simple est intuitive pour l'utilisateur.

L'utilisation de Xamarin.Forms nous permet de développer rapidement l'application tout en respectant la ligne de conduite établie par le Material Design ce qui lui donne un design propre et élégant.

L'objectif est de rapidement pouvoir récupérer des informations sur les lancements space x donc l'application a été pensée en trois pages.

### Pages

- Upcoming:
  - Permet de lister les prochain lancements Space X, si l'on selectionne un item dans la liste, un volet s'affiche pour nous afficher les détails.
- Latest:
  - Permet de lister les lancement passés Space X, si l'on sélectionne un item dans la liste, un volet s'affiche pour nous afficher les détails.

- Map:
  - Permet de nous localiser sur une carte, nous affiche également les points de lancement, quand on clique sur un point de lancement, celui si nous affiche ses détails dans une modal.

## TODO

- Charte graphique dark theme
- Améliorer la disposition des éléments
- Récupérer et afficher plus de détails
- Refactor

## Contributors

- Richard Dequeker
- Corentin Ploteau
- Kevin George

