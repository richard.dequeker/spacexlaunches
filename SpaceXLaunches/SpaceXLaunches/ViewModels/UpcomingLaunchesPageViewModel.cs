﻿using Prism.Mvvm;
using SpaceXLaunches.Views;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SpaceXLaunches.ViewModels
{
	public class UpcomingLaunchesPageViewModel : BindableBase
	{
        private ObservableCollection<Launch> _upcomingLaunches;
        public ObservableCollection<Launch> UpcomingLaunches
        {
            get => _upcomingLaunches;
            set
            {
                _upcomingLaunches = value;
                RaisePropertyChanged();
            }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get => _isLoading;
            set
            {
                _isLoading = value;
                RaisePropertyChanged();
            }
        }

        private Launch _selectedLaunch;
        public Launch SelectedLaunch
        {
            get
            {
                return _selectedLaunch;
            }
            set
            {
                _selectedLaunch = value;
                if (_selectedLaunch == null)
                {
                    return;
                }
                ItemSelectedCommand.Execute(_selectedLaunch);
            }
        }

        public Command RefreshCommand => new Command(OnRefresh);
        public Command ItemSelectedCommand => new Command(OnItemSelected);

        public UpcomingLaunchesPageViewModel()
        {
            IsLoading = true;
            UpcomingLaunches = new ObservableCollection<Launch>();
            App.SpaceXLaunchesManager.OnUpcomingLaunchesLoadEnd += (s, e) => LoadUpcomingLaunches();
        }

        ~UpcomingLaunchesPageViewModel()
        {
            App.SpaceXLaunchesManager.OnUpcomingLaunchesLoadEnd -= (s, e) => LoadUpcomingLaunches();
        }

        private async void OnRefresh()
        {
            IsLoading = true;
            await App.SpaceXLaunchesManager.RefreshLatestLaunches();
            LoadUpcomingLaunches();
        }

        private void OnItemSelected()
        {
            MainMasterDetailPage.OnLaunchSelected(this, _selectedLaunch);
        }

        private void LoadUpcomingLaunches()
        {
            UpcomingLaunches = new ObservableCollection<Launch>(App.SpaceXLaunchesManager.UpcomingLaunches);
            IsLoading = false;
        }
    }
}
