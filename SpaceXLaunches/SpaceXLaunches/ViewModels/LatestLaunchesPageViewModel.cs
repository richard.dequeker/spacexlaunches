﻿using Prism.Mvvm;
using SpaceXLaunches.Views;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;

namespace SpaceXLaunches.ViewModels
{
	public class LatestLaunchesPageViewModel : BindableBase
	{
        private ObservableCollection<Launch> _latestLaunches;
        public ObservableCollection<Launch> LatestLaunches
        {
            get => _latestLaunches;
            set
            {
                _latestLaunches = value;
                RaisePropertyChanged();
            }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get => _isLoading;
            set
            {
                _isLoading = value;
                RaisePropertyChanged();
            }
        }

        private Launch _selectedLaunch;
        public Launch SelectedLaunch
        {
            get
            {
                return _selectedLaunch;
            }
            set
            {
                _selectedLaunch = value;
                if (_selectedLaunch == null)
                {
                    return;
                }
                ItemSelectedCommand.Execute(_selectedLaunch);
            }
        }

        public Command RefreshCommand => new Command(OnRefresh);
        public Command ItemSelectedCommand => new Command(OnItemSelected);

        public LatestLaunchesPageViewModel()
        {
            IsLoading = true;
            LatestLaunches = new ObservableCollection<Launch>();
            App.SpaceXLaunchesManager.OnLatestLaunchesLoadEnd += (s, e) => LoadLatestLaunches();
        }

        ~LatestLaunchesPageViewModel()
        {
            App.SpaceXLaunchesManager.OnLatestLaunchesLoadEnd -= (s, e) => LoadLatestLaunches();
        }

        private async void OnRefresh()
        {
            IsLoading = true;
            await App.SpaceXLaunchesManager.RefreshLatestLaunches();
            LoadLatestLaunches();
        }

        private void OnItemSelected() 
        {
            MainMasterDetailPage.OnLaunchSelected(this, _selectedLaunch);
        }

        private void LoadLatestLaunches()
        {
            LatestLaunches = new ObservableCollection<Launch>(App.SpaceXLaunchesManager.LatestLaunches);
            IsLoading = false;
        }
    }
}
