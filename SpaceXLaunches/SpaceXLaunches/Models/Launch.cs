﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SpaceXLaunches
{
    public class Launch
    {
        [JsonProperty("flight_number")]
        public string Number { get; set; }

        [JsonProperty("mission_name")]
        public string Name { get; set; }

        [JsonProperty("launch_date_utc")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("rocket")]
        public Rocket Rocket { get; set; }

        [JsonProperty("launch_site")]
        public LaunchSite LaunchSite { get; set; }

        public Site Site { get; set; }

        [JsonProperty("links")]
        public Links Links { get; set; }

    }

    public class LaunchSite
    {
        [JsonProperty("site_id")]
        public string Id { get; set; }
    }

    public class Links
    {
        [JsonProperty("mission_patch")]
        public string Patch { get; set; }
    }
}