﻿using Newtonsoft.Json;

namespace SpaceXLaunches
{
    public class Site
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("location")]
        public SiteLocation Location { get; set; }
    }

    public class SiteLocation
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }
    }
}
