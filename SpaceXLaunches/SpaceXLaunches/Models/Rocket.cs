﻿using Newtonsoft.Json;

namespace SpaceXLaunches
{
    public class Rocket
    {

        [JsonProperty("rocket_name")]
        public string Name { get; set; }
    }
}
