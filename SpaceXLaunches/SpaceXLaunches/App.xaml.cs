﻿using Prism;
using Prism.Ioc;
using SpaceXLaunches.ViewModels;
using SpaceXLaunches.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SpaceXLaunches
{
    public partial class App
    {
       
        public static SpaceXLaunchesManager SpaceXLaunchesManager;

        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer)
        {
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            SpaceXLaunchesManager = new SpaceXLaunchesManager(new SpaceXLaunchesService());
            SpaceXLaunchesManager.Init();
            await NavigationService.NavigateAsync("NavigationPage/MainMasterDetailPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<LaunchTabbedPage, LaunchTabbedPageViewModel>();
            containerRegistry.RegisterForNavigation<LatestLaunchesPage, LatestLaunchesPageViewModel>();
            containerRegistry.RegisterForNavigation<UpcomingLaunchesPage, UpcomingLaunchesPageViewModel>();
            containerRegistry.RegisterForNavigation<LaunchMapPage, LaunchMapPageViewModel>();
            containerRegistry.RegisterForNavigation<MainMasterDetailPage, MainMasterDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<LaunchDetailPage, LaunchDetailPageViewModel>();
        }
    }
}
