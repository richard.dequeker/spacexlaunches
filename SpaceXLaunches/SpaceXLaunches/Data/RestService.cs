﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Diagnostics;
using Newtonsoft.Json;

namespace SpaceXLaunches
{
    public class RestService : IRestService
    {
        HttpClient _client;

        public RestService()
        {
            _client = new HttpClient();
        }

        public async Task<T> GetData<T>(Uri uri)
        {
            HttpResponseMessage response = new HttpResponseMessage();

                response = await _client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(content);
                } else
                {
                    throw new Exception($"HttpRequestException: Status code [{response.StatusCode}]");
                }
        }
    }
}
