﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpaceXLaunches
{
    public interface ISpaceXLaunchesManager
    {
        event EventHandler OnLatestLaunchesLoadEnd;
        event EventHandler OnUpcomingLaunchesLoadEnd;

        void Init();
        Task RefreshLatestLaunches();
        Task RefreshUpcomingLaunches();
    }
}
