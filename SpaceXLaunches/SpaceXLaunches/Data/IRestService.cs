﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SpaceXLaunches
{
    public interface IRestService
    {
        Task<T> GetData<T>(Uri uri);
    }
}
