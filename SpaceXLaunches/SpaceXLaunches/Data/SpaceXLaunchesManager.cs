﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SpaceXLaunches
{
    public class SpaceXLaunchesManager : ISpaceXLaunchesManager
    {
        public event EventHandler OnLatestLaunchesLoadEnd;
        public event EventHandler OnUpcomingLaunchesLoadEnd;

        public List<Launch> LatestLaunches;
        public List<Launch> UpcomingLaunches;
        private ISpaceXLaunchesService _restService;

        public SpaceXLaunchesManager(ISpaceXLaunchesService service)
        {
            _restService = service;
            LatestLaunches = new List<Launch>();
            UpcomingLaunches = new List<Launch>();
        }

        public void Init()
        {
            var tasks = new List<Task>()
            {
                Task.Factory.StartNew(RefreshLatestLaunches),
                Task.Factory.StartNew(RefreshUpcomingLaunches)
            };
            try
            {
                Task.WaitAll(tasks.ToArray());
            }
            catch(Exception ex)
            {
                Debug.WriteLine($"SpaceXLaunchesManager: {ex.Message}");
            }
        }

        public async Task RefreshLatestLaunches()
        {
            LatestLaunches = await _restService.GetManyLaunches("past");
            OnLatestLaunchesLoadEnd?.Invoke(this, EventArgs.Empty);
        }

        public async Task RefreshUpcomingLaunches()
        {
            UpcomingLaunches = await _restService.GetManyLaunches("upcoming");
            OnUpcomingLaunchesLoadEnd?.Invoke(this, EventArgs.Empty);
        }
    }
}
