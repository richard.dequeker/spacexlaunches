﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpaceXLaunches
{
    public interface ISpaceXLaunchesService : IRestService
    {
        Task<List<Launch>> GetManyLaunches(string param);
        Task<Site> GetOneSite(string param);
    }
}
