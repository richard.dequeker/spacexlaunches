﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpaceXLaunches
{
    public class SpaceXLaunchesService : RestService, ISpaceXLaunchesService
    {
        public async Task<List<Launch>> GetManyLaunches(string param)
        {
            List<Launch> launches = new List<Launch>();
            Uri uri = new Uri($"{Constants.SpaceXApiBaseAddress}/launches/{param}/");
            launches = await GetData<List<Launch>>(uri);
            foreach (Launch launch in launches)
            {
                launch.Site = await GetOneSite(launch.LaunchSite.Id);
            }
            return launches;
        }

        public async Task<Site> GetOneSite(string param)
        {
            Uri uri = new Uri($"{Constants.SpaceXApiBaseAddress}/launchpads/{param}/");
            var data = await GetData<Site>(uri);
            return data;
        }
    }
}
