﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace SpaceXLaunches.Views
{
    public partial class LaunchMapPage : ContentPage
    {
        Xamarin.Forms.Maps.Map mapView;
        Position mapPosition;
        Location userLocation;

        public LaunchMapPage()
        {
            InitializeComponent();
            mapView = new Xamarin.Forms.Maps.Map(
                MapSpan.FromCenterAndRadius(new Position(37, -122), Distance.FromMiles(0.3))
            )
            {
                IsShowingUser = true,
                HeightRequest = 100,
                WidthRequest = 960,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            var stack = new StackLayout { Spacing = 0 };
            stack.Children.Add(mapView);
            Content = stack;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await GetUserLocation();
            PopulateMap();
            mapView.MoveToRegion(MapSpan.FromCenterAndRadius( mapPosition, Distance.FromMiles(1)));
        }

        async Task GetUserLocation()
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Best);
                userLocation = await Geolocation.GetLastKnownLocationAsync();
                userLocation = await Geolocation.GetLocationAsync(request);
                mapPosition = new Position(userLocation.Latitude, userLocation.Longitude);
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                Debug.WriteLine(fnsEx);
            }
            catch (PermissionException pEx)
            {
                Debug.WriteLine(pEx);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void PopulateMap()
        {
            var myPin = new Pin
            {
                Type = PinType.Place,
                Position = mapPosition,
                Label = "You are here",
            };
            mapView.Pins.Add(myPin);
            foreach (Launch launch in App.SpaceXLaunchesManager.LatestLaunches)
            {
                mapView.Pins.Add(GenerateLaunchPin(launch));
            }
            foreach (Launch launch in App.SpaceXLaunchesManager.UpcomingLaunches)
            {
                mapView.Pins.Add(GenerateLaunchPin(launch));
            }
        }

        private Pin GenerateLaunchPin(Launch launch)
        {
            var launchPosition = new Position(launch.Site.Location.Latitude, launch.Site.Location.Longitude);
            var launchPin = new Pin
            {
                Type = PinType.Place,
                Position = launchPosition,
                Label = launch.Site.Location.Name
            };
            return launchPin;
        }
    }

}
