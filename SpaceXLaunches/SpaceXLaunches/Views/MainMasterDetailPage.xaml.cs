﻿using System;
using Xamarin.Forms;

namespace SpaceXLaunches.Views
{
    public partial class MainMasterDetailPage : MasterDetailPage
    {
        public static EventHandler<Launch> OnLaunchSelected;

        public MainMasterDetailPage()
        {
            InitializeComponent();
            OnLaunchSelected += (s, e) => OnLaunchSelectedHandler(s, e); 
        }

        private void OnLaunchSelectedHandler(object sender, Launch launch)
        {
            ContructLaunchDetailView(launch);
            IsPresented = true;
        }

        private void ContructLaunchDetailView(Launch launch)
        {
            layout.Children.Clear();

            // HEADER
            StackLayout headerLayout = new StackLayout()
            { 
                HeightRequest = 100,
                Spacing = 10,
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };
            if (launch.Links.Patch != null)
            {
                Image patch = new Image()
                {
                    Source = launch.Links.Patch,
                    HeightRequest = 60,
                    WidthRequest = 60,
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Start,
                };
                headerLayout.Children.Add(patch);
            }
            Label title = new Label()
            {
                Text = launch.Name,
                FontSize = 28,
                HorizontalOptions = LayoutOptions.Center
            };
            Label subTitle = new Label()
            {
                Text = launch.Rocket.Name,
                TextColor = Color.LightGray,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            headerLayout.Children.Add(title);
            
            // INFOS
            StackLayout infoLayout = new StackLayout();
            // date
            StackLayout dateLayout= new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
            };
            Label dateLabel = new Label() { Text = "Date: " };
            Label dateValue = new Label() { Text = launch.Date.ToString("dd/mm/yyyy") };
            dateLayout.Children.Add(dateLabel);
            dateLayout.Children.Add(dateValue);
            // site
            StackLayout siteLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
            };
            Label siteLabel = new Label() { Text = "Site: " };
            Label siteValue = new Label() { Text = launch.Site.Location.Name };
            siteLayout.Children.Add(siteLabel);
            siteLayout.Children.Add(siteValue);
            //
            infoLayout.Children.Add(dateLayout);
            infoLayout.Children.Add(siteLayout);
            // Finally
            layout.Children.Add(headerLayout);
            layout.Children.Add(infoLayout);
        }
    }
}